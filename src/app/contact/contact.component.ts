import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormBuilder} from '@angular/forms';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  FormData!: FormGroup;
  isLoading: boolean = false;
  isSubmitted: boolean = false;
  responseMsg: string = ""

  constructor(private builder: FormBuilder, private contact: ContactService) { }

  ngOnInit(): void {
    //this.message.setValue("I'd like to have a quote for full house painting")
    this.FormData = this.builder.group({
      Name: new FormControl('', [Validators.required]),
      Phone: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/[0-9 ]+/)])),
      Email: new FormControl(''),
      Address: new FormControl('', [Validators.required]),
      Message: new FormControl('', Validators.required)
    })
  }

  onSubmit(FormData: any) {
    console.log(FormData)
    this.isLoading = true
    this.isSubmitted = false
    this.contact.PostMessage(FormData).subscribe(
      (response:any) => {
        if (response["result"] == "success") {
          this.responseMsg = "Thanks for contacting us. We'll get back to within 48 hours!"
        } else {
          this.responseMsg = "Something went wrong...Please reload the page and try again."
        }
        this.FormData.enable()
        this.isSubmitted = true
        this.isLoading = false;
      }
    )

  }

}
