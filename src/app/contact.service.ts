import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private mailApi = "https://script.google.com/macros/s/AKfycbxfsdnkJ4__8lp60NV7lSSHp96M3oRpNDhYnsLbSFNzq8JRcS_VrGiPT0-KeDt24EIHKA/exec"

  constructor(private http: HttpClient) { }

  PostMessage(input: any) {
    var subject = "Quote request for " + input.Address
    var formData: any = new FormData();
    formData.append("name", input.Name);
    formData.append("phone", input.Phone);
    formData.append("email", input.Email);
    formData.append("address", input.Address);
    formData.append("message", input.Message);
    return this.http.post(this.mailApi, formData)
  }
}
